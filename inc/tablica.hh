#ifndef TABLICA_HH
#define TABLICA_HH
#include<iostream>
#include<fstream>
#include<string>
using namespace std;

struct tablica
{

  int** element;
  int i_kolumn;
  int i_rzedow;

  tablica(int m=1, int n=1);
  tablica(const tablica& tab);
  ~tablica();

  void wypelnij(int x);
  int max();

  friend ostream& operator <<(ostream& out_strm, const tablica &tab);
  friend istream& operator >>(istream& in_strm, tablica &tab);
};

int fload(string fname,tablica &tab);
int fsave(string fname,tablica tab);
int bin_fload(string fname,tablica &tab);
int bin_fsave(string fname,tablica tab);

#endif
