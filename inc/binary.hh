#ifndef BINARY_HH
#define BINARY_HH
#include<iostream>
using namespace std;

struct bin
{

  bool *number;
  int i_bit;

  bin(int q=1);
  friend istream& operator >> (istream& in_strm, bin& num);
  friend ostream& operator << (ostream& out_strm, const bin& num);
};

int bin_to_int(bin num);
bin int_to_bin(int num);

#endif
