#include"binary.hh"

bin::bin(int q)
{
  number=new bool[q]; 
  i_bit=q;
  for(int i=0;i<i_bit;++i)
    number[i]=0;
}

int bin_to_int(bin num)
{
  int int_num=0;
  int quant=1;
  for(int i=0;i<num.i_bit;++i)
    {
      int_num+=num.number[i]*quant;
      quant*=2;
    }
  return int_num;
}

bin int_to_bin(int num)
{
  int i;
  int quant=1;
  if(num==0)
    {
      bin val;
      return val;
    }
  else
    {
      for(i=0;num>=quant;++i)
	quant*=2;
      quant/=2;
      num-=quant;
      bin val(i);
      val.number[i]=1;
      
      while(num>0)
	{
	  quant=1;
	  for(i=0;num>=quant;++i)
	    quant*=2;
	  quant/=2;
	  num-=quant;
	  val.number[i]=1;
	}
      return val;
    }

}

istream& operator >> (istream& in_strm, bin& num)
{
  int i;
  char buff='x';
  for(i=0;buff!='|';++i)
    {
      in_strm>>buff;
      cerr<<buff;
    }
  num=bin(i);
  in_strm.clear();
  in_strm.seekg(0,ios::beg);
  for(i=num.i_bit;i>0;--i)
    {
      in_strm>>num.number[i];
      in_strm.clear();
    }
  return in_strm;
}

ostream& operator << (ostream& out_strm, const bin& num)
{
  for(int i=num.i_bit;i>0;--i)
    out_strm<<num.number[i];
  out_strm<<'|';
  return out_strm;
}


