#include<iostream>
#include<fstream>
#include"binary.hh"
#include"tablica.hh"
#include"funkcje.hh"
//using namespace std;

int main()
{
  tablica tab1;
  int opcja=12;
  /* tablica tab2(2);
  tablica tab3(2,3);
  tab1.wypelnij(100);
  tab2.wypelnij(50);
  tab3.wypelnij(20);
  fload("przyklad_tab",tab1);
  cout<<"max1= "<<tab1.max()<<'\t'
      <<"max2= "<<tab2.max()<<'\t'
      <<"max3= "<<tab3.max()<<endl;
  cout<<"tab1"<<endl<<tab1<<endl
      <<"tab2"<<endl<<tab2<<endl
      <<"tab3"<<endl<<tab3<<endl;
  fsave("przyklad2_tab",tab3);
  cout<<Potega(4,3)<<endl;
  cout<<Silnia(3)<<endl;
  cout<<jestPal("1234321")<<endl;

  int liczba=tab2.element[0][0];
  bin b_liczba=int_to_bin(liczba);
  cout<<b_liczba<<endl;
  
  bin_fsave("przyklad_bin",tab3);
  tablica tab4;
  bin_fload("przyklad_bin",tab4);
  cout<<tab4<<endl;
  */

  while(opcja !=0)
    {
      cout <<endl<<endl<< "-------------------------------------"<<endl;
      cout << "Dostepne opcje"<<endl;
      cout << "1.Stworzenie tablicy o wymiarach m x n"<<endl;
      cout << "2.Wypelnienie tablicy losowymi liczbami z przedzialu [0;X]"<<endl;
      cout << "3.Wy�wietlenie tablicy"<<endl;
      cout << "4.Wczytywanie z pliku tekstowego"<<endl;
      cout << "5.Zapisywanie do  pliku tekstowego"<<endl;
      cout << "6.Wczytywanie z pliku binarnego"<<endl;
      cout << "7.Zapisywanie do  pliku binarnego"<<endl;
      cout << "8.Wyswietl maxymalny element tablicy"<<endl;
      cout << "9.Oblicz x^n"<<endl;
      cout << "10.Oblicz x!"<<endl;
      cout << "11.Sprawdz czy wyraz jest palindromem"<<endl;
      cout << "0.Koniec"<<endl;
      cout << "-------------------------------------"<<endl<<endl;
      cout << "Twoj wybor: ";
      cin >> opcja;
      switch(opcja)
	{
	case 0:
	  return 0;
	  break;
	case 1:
	  {
	    int m,n;
	    cout<<"Podaj wymiar m: ";
	    cin >> m;
	    cin.clear();
	    cout<<"Podaj wymiar n: ";
	    cin >> n;
	    cin.clear();
	    tab1=tablica(m,n);	    
	  } break;
	case 2:
	  {
	    int X;
	    cout<<"Podaj granice przedzialu X: ";
	    cin >> X;
	    cin.clear();
	    tab1.wypelnij(X);
	  }break;
	case 3:
	  {
	    cout<<endl<<tab1<<endl;
	  }break;
	case 4:
	  {
	    string fname;
	    cout<<"Podaj nazw� pliku: ";
	    cin >> fname;
	    cin.clear();
	    fload(fname,tab1);
	  }break;
	case 5:
	  {
	    string fname;
	    cout<<"Podaj nazwe pliku: ";
	    cin >> fname;
	    cin.clear();
	    fsave(fname,tab1);
	  }break;
	case 6:
	  {
	    string fname;
	    cout<<"Podaj nazwe pliku: ";
	    cin >> fname;
	    cin.clear();
	    bin_fload(fname,tab1);
	  }break;
	case 7:
	  {
	    string fname;
	    cout<<"Podaj nazwe pliku: ";
	    cin >> fname;
	    cin.clear();
	    bin_fsave(fname,tab1);
	  }break;
	case 8:
	  {
	    cout<<"Max: "<<tab1.max()<<endl;
	  }break;
	case 9:
	  {
	    int x;
	    int n;
	    cout<<"Podaj x: ";
	    cin>>x;
	    cin.clear();
	    cout<<"Podaj n: ";
	    cin>>n;
	    cin.clear();
	    cout<<"Wynik= "<<Potega(x,n)<<endl;
	  }break;
	case 10:
	  {
	    int x;
	    cout<<"Podaj x: ";
	    cin>>x;
	    cin.clear();
	    cout<<"Wynik= "<<Silnia(x)<<endl;
	  }break;
	case 11:
	  {
	    string name;
	    cout<<"Podaj slowo: ";
	    cin>>name;
	    cin.clear();
	    if(jestPal(name)) cout<<"Jest palindromem"<<endl;
	    else cout<<"Nie jest palindromem"<<endl;
	  }break;
	default:
	  return 0;
	}
    }
}
