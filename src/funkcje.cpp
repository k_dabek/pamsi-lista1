#include "funkcje.hh"

int Potega(int x, int p)
{
  if(p>1)
    x*=Potega(x,p-1);
  return x;
}

int Silnia(int x)
{
  if(x>13) 
    {
      cerr<<"Zbyt duza liczba dla silni"<<endl;
      return 0;
    }
  if(x>1)
    x*=Silnia(x-1);
  return x;
}

bool jestPal(string testStr)
{
  if(testStr.at(0)!=testStr.back())
    return false;
  else
    if(testStr.length()<=2)
      return true;
    else
      return jestPal(testStr.substr(1,testStr.length()-2));
}
