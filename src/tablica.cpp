#include"tablica.hh"
#include"binary.hh"

tablica::tablica(int m, int n)
{
  element=new int*[m];
  for(int i=0; i<m; ++i)
    element[i]=new int[n];
  i_kolumn=m;
  i_rzedow=n;
}

tablica::tablica(const tablica& tab)
{
  element=tab.element;
  i_kolumn=tab.i_kolumn;
  i_rzedow=tab.i_rzedow;
}

tablica::~tablica()
{/*
  for(int j=0;j<i_kolumn;++j)
    delete [] element[j];
    delete [] element;*/
}

void tablica::wypelnij(int x)
{
  srand(time(NULL));
  for(int i=0; i<i_kolumn; ++i)
    for(int j=0; j<i_rzedow; ++j)
      {
	element[i][j]=rand() % (x+1);
      }
}

int tablica::max()
{
  int val_max=element[0][0];
  for(int i=0; i<i_kolumn; ++i)
    for(int j=0; j<i_rzedow; ++j)
      if(element[i][j]>val_max)
	val_max=element[i][j];
  return val_max;
}

ostream& operator <<(ostream& out_strm, const tablica &tab)
{
  for(int i=0; i<tab.i_kolumn; ++i)
    {
      for(int j=0; j<tab.i_rzedow; ++j)
	out_strm<<tab.element[i][j]<<'\t';
      out_strm<<endl;
    }
  return out_strm;
}

istream& operator >>(istream& in_strm, tablica &tab)
{
  string check;
  int buff=0,m=0,n=0;
  while(!in_strm.eof())
    {
      in_strm>>buff;
      ++n;
    }
  in_strm.clear();
  in_strm.seekg(0,ios::beg);
  while(!in_strm.eof())
    {
      getline(in_strm,check);
      ++m;
    }
  --m;
  n/=m;
  tab=tablica(m,n);
  in_strm.clear();
  in_strm.seekg(0,ios::beg);
  for(int i=0;i<m;++i)
    for(int j=0;j<n;++j)
      {
	in_strm>>buff;
	tab.element[i][j]=buff;
	in_strm.clear();
      }
  return in_strm;
}

int fload(string fname,tablica &tab)
{
  ifstream file_strm;
  file_strm.open(fname);
  if(!file_strm.is_open())
    {
      cerr<<"Blad otwarcia pliku"<<endl;
      return 1;
    }
  file_strm>>tab;
  if(file_strm.fail())
    {
      cerr<<"Blad odczytu danych z pliku"<<endl;
      return 2;
    }
  file_strm.close();
  return 0;
}


int fsave(string fname, tablica tab)
{
  ofstream file_strm;
  file_strm.open(fname);
  if(!file_strm.is_open()) 
  {
    cerr<<"Blad otwarcia pliku"<<endl;
    return 1;
  }
  file_strm<<tab;
  if(file_strm.fail()) 
  {
    cerr<<"Blad zapisu danych z pliku"<<endl;
    return 2;
  }
  file_strm.close();
  return 0;
}


int bin_fsave(string fname,tablica tab)
{
  ofstream file_strm(fname,ios::binary);
  int buff;
  if(!file_strm.is_open()) 
  {
    cerr<<"Blad otwarcia pliku"<<endl;
    return 1;
  }
  
  for(int i=0;i<tab.i_kolumn;++i)
    {
      for(int j=0;j<tab.i_rzedow;++j)
	{
	  buff=tab.element[i][j];
	  file_strm.write((char*)&buff,sizeof(int));
	}
    }
  
  if(file_strm.fail()) 
    {
      cerr<<"Blad zapisu danych z pliku"<<endl;
      return 2;
    }
  file_strm.close();
  return 0;
  
}


int bin_fload(string fname, tablica &tab)
{
  ifstream file_strm(fname,ios::binary);
  char buff[sizeof(int)];
  streampos f_size;
  /******/
  f_size=file_strm.tellg();
  file_strm.seekg(0,ios::end);
  f_size=file_strm.tellg()-f_size;
  f_size= (int)f_size/sizeof(int);
  file_strm.seekg(0,ios::beg);
/******/
  tab=tablica(1,f_size);
  file_strm.clear();
  file_strm.seekg(0,ios::beg);
  for(int i=0;i<tab.i_kolumn;++i)
    for(int j=0;j<tab.i_rzedow;++j)
      {
	file_strm.read(buff,sizeof(int));
	int* val=(int*) buff;
	tab.element[i][j]=*val;
      }
  /******/
  return 0;
}
