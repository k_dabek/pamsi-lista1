NAME = ./bin/Lista1

OBJECTS = *.o
SOURCE = *.cpp
INCLUDES = *.hh

BINDIR = bin
OBJDIR = obj
SRCDIR = src
INCDIR = inc

OBJECTS := ./$(OBJDIR)/$(OBJECTS)
SOURCE := ./$(SRCDIR)/$(SOURCE)
INCLUDES := ./$(INCDIR)/$(INCLUDES)

CPP = g++
CPPFLAGS = -pedantic -Wall -std=c++11 -I inc -g 2>&1
LINKER = g++ 
LFLAGS =

$(NAME): $(OBJECTS)
	$(LINKER) $(OBJECTS) $(LFLAGS) -o $@ 

$(OBJECTS): $(SOURCE) $(INCLUDES)
	$(CPP) -c $(SOURCE) $(CPPFLAGS) 
	mv *.o $(OBJDIR)

clean:
	rm -f $(OBJECTS)
.PHONY: clean
